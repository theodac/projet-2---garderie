
<div class="row">
    <div class="col l2 s12">
        <form class="" action="" method="post">
            <div class="row">
                <div class="col l12 s6">
                    <!-- filtre : géographique-->
                    <div class="slidecontainer">
                        <p>Distance : <span id="demo"></span>km
                            <input type="range" min="1" max="100" value="50" class="slider responsive" id="myRange" style="height:50px">
                        </p>
                    </div>
                </div>
                <div class="col l12 s3">
                    <select class="marginT45" name="select" id="select">
                        <option value="priceMax">Plus cher</option>
                        <option value="priceMin">Moins cher</option>
                        <option value="placeMax">Plus de place disponnible</option>
                        <option value="placeMin">Moins de place disponnible</option>
                    </select>
                </div>
                <div class="col l12 s3">
                    <button class="btn waves-effect waves-light marginT45" type="submit" name="action">Trier
                        <i class="material-icons right">send</i>
                    </button>
                </div>
            </div>
        </form>
    </div>

    <div class="container">
        <div class="section">
            <!--   Icon Section   -->
            <div class="row">
                <?php
                foreach ($test as $value)
                {
                    $str = $value["Name"]."_".$value['ID'];
                    $lastID = $value["ID"];
                    ?>
                <div class="col s12 m4">

                    <div class="card">


                        <div class="card-image" style="max-height: 250px;">
                            <img src="<?= PUB_PATH?>/public/img<?= DIRSEP;?>creche1.jpg">
                        </div>
                        <div class="card-content">
                            <h5 class="titre"><?= $value["Name"] ?></h5>
                            <p> Notre crèche située à <?= $value["Address"]?> accueille les enfants en bas age de <?= $value["MinAge"]?> mois à <?= $value["MaxAge"]?> mois. </p>
                            <p> Vous pouvez nous contacter au : <?= $value["Phone"]?> ou par mail <?=$value["Email"]?>. </p>
                            <p> Notre capacité d'accueille est de  <?= $value["Capacity"]?> places. </p>
                            <p> Notre tarif horaire est de <?= $value['Price']?>€. </p>
                            <p> Numero de siret : <?= $value["SIRET"]?> .</p>
                            <?php
                            if (isset($distance)){
                            ?>

                            <p style="font-weight: bold;"> Cette creche est à <?= $distance[$lastID]?> km de chez vous. </p>
                            <?php
                            }
                            ?>
                        </div>
                        <div class="card-action">
                            <a href='<?= PUB_PATH ?>/detail/index/<?=$slug->gen_slug($str)?>'>Detail</a>
                        </div>
                    </div>

                </div>
                <?php

                }
                ?>
            </div>
        </div>
    </div>
</div>
</div>





<?php
if ($thispage < $pageMax)
{
    $nextpage = $thispage+1;
    echo "<a href='".PUB_PATH."/recherche/index/".$nextpage."' class='right-align'>Suivante</a>";
}

if ($thispage > 1)
{
    $prevpage = $thispage-1;
    echo "<a href='".PUB_PATH."/recherche/index/".$prevpage."' class='left-align'>Precedente</a>";
}

?>
<script type="text/javascript">
    // filte géographique js
    var slider = document.getElementById("myRange");
    var output = document.getElementById("demo");
    output.innerHTML = slider.value;
    slider.oninput = function()
    {
        output.innerHTML = this.value;
    }
</script>
