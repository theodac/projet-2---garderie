<?php

$getParamUrl= $_SERVER['REQUEST_URI'];
$getParamUrlArray = explode('_',$getParamUrl);
$id = $getParamUrlArray[1];


$sql = new Model();
$sql = $sql->select('professional','WHERE ID = '.$id);
$sql = $sql->fetch();
?>
<div class="content">
    <div class="content">
        <div class="row">
            <div class='main-content' id="mainC">
                <div class="col s12 m4 l2"></div>
                <div class="col s12 m4 l4">
                    <br>
                    <br>
                    <img class="pro" src='https://i.unimedias.fr/2018/01/04/creche.jpg?auto=format%2Ccompress&crop=faces&cs=tinysrgb&fit=crop&h=227&w=405' alt='crecheimg' class='responsive-img'">
                </div>
                <div class="col s12 m4 l4">
                    <h3 class='center-align'><?= $sql['Name'] ?></h3>
                    <p>Notre crèche située  <?=$sql['Address']?> accueille les enfants en bas age de <?=$sql['MinAge']?> ans à <?=$sql['MaxAge']?>  ans. </p>
                    <p>Vous pouvez nous contacter au : <?=$sql['Phone']?> ou par mail <?=$sql['Email']?>. </p>

                    <ul>
                        <li>
                            Capacité  : <?= $sql['Capacity']?>
                        </li>
                        <li>
                            Prix :  <?= $sql['Price']?> €/h
                        </li>
                        <li>
                            Horaire : 9h - 18h
                        </li>
                    </ul>
                </div>
                <div class="col s12 m4 l2"></div>
            </div>
        </div>


        <!-- button -->
        <div class="row">

            <div class="col s12 m4 l2"><p></p></div>
            <div class="col s12 m4 l2"><p></p></div>
            <?php
            if(isset($_SESSION['ID'])){
                ?>
                <div class="col s12 m4 l2">
                    <form action="#" method="post">
                        <button name="chat" class='waves-effect waves-light btn-large  center alig'>D'autres question ?</button>
                    </form>
                </div>
                <div class="col s12 m4 l2">
                    <div id="btnContainer">
                        <a id="chatBtn" class='waves-effect waves-light btn-large center alig'>Reservez</a>
                    </div>
                </div>
                <?php
            }
            ?>
            <div class="col s12 m4 l2"><p></p></div>
            <div class="col s12 m4 l2"><p></p></div>
        </div>

        <!--
                <div class='center-align btn_css'>
                    <form action="#" method="post">
                        <button name="chat" class='waves-effect waves-light btn-large  center alig'>Reservez</button>
                    </form>
            </div>-->
        <div id="reserve" class='center-align btn_css' style="display: none;">



        <?php
        if(isset($_SESSION['ID'])){
            ?>
            <div class='center-align btn_css'>

                <!--            <div id="chatContainer">
                                <div id="btnContainer">
                                    <a id="chatBtn" class='waves-effect waves-light btn-large center alig'>Contacter la garderie</a>
                                </div>
                                <div id="chatW" style="display: none">
                                    <div id="chatBody" style="display: none">
                                        <div id="msg">
                                            <div>
                                                <p class="msgUsr">Bonjour, je souhaiterai inscrire mon enfant</p>
                                            </div>
                                            <div>
                                                <p class="msgPro">Bien sûr ! Ce serait pour quelle période ?</p>
                                            </div>
                                        </div>
                                        <div id="chatInput" style="display: none">
                                            <input type="text" placeholder="Tapez votre message..." />
                                        </div>
                                    </div>

                                </div>
                           </div>-->




                <table>
                    <caption><h4>Planning de disponibilitées </h4> </caption>
                    <thead>
                    <tr>

                        <th>Date de début</th>
                        <th scope="col">Date de fin </th>
                        <th scope="col">Place restante </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>

                        <?php

                        foreach ($aval as $key => $value) {
                            echo"<tr>
                            <td >".
                                $value['TimeBeg']
                                ."</td >
                            <td >".
                                $value['TimeEnd']
                                ."</td >
                            <td >".
                                $value['AvailableCap']
                                ."</td >
                           </tr >";

                        }
                        ?>

                    </tr>
                    </tbody>
                </table>



                <br>


                <div class="row">
                    <form method="post" action="#">


                        <div class="col s6">
                            <p>Selectionnez l'enfant que vous souhaitez inscrire </p>

                            <select name="Enfant">

                                <?php

                                foreach ($child as $key => $value) {
                                    echo"<option value='".$value['ID']."'>"
                                        .$value['Firstname']
                                        ."</option>";

                                }
                                ?>
                                </option>
                            </select>
                        </div>


                        <div class="col s6">

                            <p>Selectionnez votre créneau horaire</p>

                            <select name="Time">

                                <?php

                                foreach ($aval as $key => $value) {
                                    echo"<option>".
                                        $value['TimeBeg'].' - '. $value['TimeEnd']
                                        ."</option>";

                                }
                                ?>

                            </select>

                        </div>

                        <button class="btn waves-effect waves-light" id="submitBtn" type="submit" name="action">Validez
                            <i class="material-icons right">send</i>
                        </button>
                    </form>
                </div>
            </div>
            <?php
        }
        ?>
    </div>
    </div>