

<div class="carousel carousel-slider center" data-indicators="true">

    <div class="carousel-item red white-text" href="#one!">
        <img src="<?= PUB_PATH?>/public/img<?= DIRSEP;?>enfant.jpg">
    </div>
    <div class="carousel-item amber white-text" href="#two!">
        <img src="<?= PUB_PATH?>/public/img<?= DIRSEP;?>creche2.jpg">
    </div>
    <div class="carousel-item green white-text" href="#three!">
        <img src="<?= PUB_PATH?>/public/img<?= DIRSEP;?>creche3.jpg">
    </div>
    <div class="carousel-item blue white-text" href="#four!">
        <img src="<?= PUB_PATH?>/public/img<?= DIRSEP;?>creche.jpg">
    </div>
</div>

<div class="container">
    <div class="section">

        <!--   Icon Section   -->
        <div class="row">
            <div class="col s12 m4">
                <div class="icon-block">
                    <h2 class="center brown-text"><img src="<?= PUB_PATH?>/public/img<?= DIRSEP;?>ours.png"></h2>
                    <h4 class="center">Qui sommes-nous ?</h4>

                    <p class="light">Notre équipe de professionnels est dédiée à 100% à la sécurité et à l’éveil de votre enfant !</p>
                    <p>Pour que petits et grands abordent l’arrivée avec sérénité. Pendant que votre enfant découvre ce nouvel environnement
                        sous l’œil attentif de l’équipe, vous pourrez nous transmettre vos attentes et poser les dernières questions avant de partir en toute tranquillité.</p>

                </div>
            </div>

            <div class="col s12 m4">
                <div class="icon-block">
                    <h2 class="center brown-text"><img src="<?= PUB_PATH?>/public/img<?= DIRSEP;?>toupie.png"></h2>
                    <h4 class="center">Un encadrement sécurisé et sécurisant</h4>

                    <p class="light"> Notre devoir est de garantir la sécurité tant physique qu’affective de votre enfant. Cette stabilité lui permettra de
                        développer : confiance en soi, ouverture aux autres et la curiosité nécessaire à ses apprentissages et acquisitions. Pour cela, nous avons instauré des « repères »
                        : matériels, temporels et humains. Notre souhait est de favoriser le bien-être de votre enfant en lui créant des rituels.</p>
                </div>
            </div>

            <div class="col s12 m4">
                <div class="icon-block">
                    <h2 class="center brown-text"><img src="<?= PUB_PATH?>/public/img<?= DIRSEP;?>cheval.png"></h2>
                    <h4 class="center">Un éveil varié et ouvert sur l’extérieur</h4>

                    <p class="light">L’alternance de jeux libres et dirigés autour de propositions éducatives riches et variées,
                        donneront à votre enfant la possibilité d’exprimer son imagination et sa créativité. Pour lui permettre de s’épanouir,
                        nous organiserons également des sorties en extérieur !
                    </p>
                </div>
            </div>
        </div>

    </div>
</div>



<div class="row">
    <div class="col s12"><p></p></div>
    <div class="col s12 m4 l2"></div>
    <div class="col s12 m4 l4 image "><img src="<?= PUB_PATH?>/public/img<?= DIRSEP;?>crecheaccueil.jpeg"></div>
    <div class="col s12 m4 l4"><p>
        <h4 class="accueil">Trouvez une place en crèche près de chez vous</h4>

        Localisez nos crèches ainsi que les crèches de nos partenaires sur une carte interactive à l’aide de votre code postal ou votre ville. Cliquez sur les crèches pour plus d’informations.
        </p><p>
            Pour être admis en crèche, votre enfant doit :
            avoir entre 2 mois et 3 ans et être en règle au regard des vaccinations obligatoires (sauf contre-indication attestée par la présentation d'un certificat médical).
        </p></div>
    <div class="col s12 m4 l2"></div>
</div>

<div class="row">
    <div class="col s12"><p></p></div>
    <div class="col s12 m4 l2"></div>
    <div class="col s12 m4 l4">
        <p>
        <h4 class="amenagement">Aménagement des crèches</h4>

        Nos crèches sont des lieux d’accueil doux, sécurisés et éco-responsables conçus dans un esprit cocon « comme à la maison » : couleurs douces, matériaux naturels…
        Les repas de la crèche sont conçus avec le plus grand soin par une diététicienne. Ils sont composés au minimum de 50 % de produits bio et d’aliments frais issus de circuits courts (moins de 200 km de la crèche).
        Les viandes sont toutes d’origine française et la composition des plats préparés est garantie sans OGM.
        </p></div>
    <div class="col s12 m4 l4 image"><img src="<?= PUB_PATH?>/public/img<?= DIRSEP;?>amenagement.jpg"></div>
    <div class="col s12 m4 l2"></div>
</div>

<div class="row">
    <div class="col s12"><p></p></div>
    <div class="col s12 m4 l2"></div>
    <div class="col s12 m4 l4 image"><img src="<?= PUB_PATH?>/public/img<?= DIRSEP;?>idee.jpg"></div>
    <div class="col s12 m4 l4">
        <p>
        <h4 class="vie">La vie dans nos crèches</h4>

        MyCreche accompagne le développement psychomoteur, social et affectif des enfants dans un environnement sécurisé et pensé pour eux.
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis dui quis diam sodales, vehicula blandit odio euismod.
        Etiam in nulla ac urna ornare ultrices eu non eros. Cras consequat mauris rutrum tempus efficitur. Vestibulum posuere felis non tincidunt tempor.
        Suspendisse potenti. Vestibulum sed sem eget ante dignissim volutpat.
        Pellentesque scelerisque ex eget augue blandit vulputate. Aenean maximus mauris quis odio euismod, at viverra tellus porta. Aenean efficitur laoreet augue.
        </p></div>

</div>
<div class="row">


    <div class="container wa" id="p360">
        <h4 class="title">    Visitez une de nos crèches comme si vous y étiez ! </h4>
        <div class="row">
            <div class="col sm 12">
                <div class="embed-responsive embed-responsive-16by9 video" style="margin-left: 15%;">
                    <iframe class="embed-responsive-item" src="https://maisonbleue.360experience.fr" style="width: 900px; height: 500px" ></iframe>
                </div>
            </div>
        </div>
    </div>
</div>

<!---->
<!--<div class="row">-->
<!--    <div class="col s12"><p></p></div>-->
<!--    <div class="col s12 m4 l1"></div>-->
<!--    <div class="col s12 m4 l4 image "><img src="--><?//= PUB_PATH?><!--/public/img--><?//= DIRSEP;?><!--crecheaccueil.jpeg"></div>-->
<!--    <div class="col s12 m4 l2"></div>-->
<!--    <div class="col s12 m4 l4"><p>-->
<!--        <h4 class="accueil">Trouvez une place en crèche près de chez vous</h4>-->
<!---->
<!--        Localisez nos crèches ainsi que les crèches de nos partenaires sur une carte interactive à l’aide de votre code postal ou votre ville. Cliquez sur les crèches pour plus d’informations.-->
<!--        </p></div>-->
<!---->
<!--</div>-->
<!---->
<!---->
<!--<div class="row">-->
<!--    <div class="col s12"><p></p></div>-->
<!--    <div class="col s12 m4 l1"></div>-->
<!--    <div class="col s12 m4 l4"><p>-->
<!--        <p>-->
<!--        <h4 class="amenagement">Aménagement des crèches</h4>-->
<!--        Nos crèches sont des lieux d’accueil doux, sécurisés et éco-responsables conçus dans un esprit cocon « comme à la maison » : couleurs douces, matériaux naturels…-->
<!--        </p></div>-->
<!--    <div class="col s12 m4 l1"></div>-->
<!--    <div class="col s12 m4 l4 image"><img src="--><?//= PUB_PATH?><!--/public/img--><?//= DIRSEP;?><!--amenagement.jpg"></div>-->
<!--    <div class="col s12 m4 l1"></div>-->
<!---->
<!--</div>-->
<!---->
<!--<div class="row">-->
<!--    <div class="col s12"><p></p></div>-->
<!--    <div class="col s12 m4 l1"></div>-->
<!--    <div class="col s12 m4 l4 image"><img src="--><?//= PUB_PATH?><!--/public/img--><?//= DIRSEP;?><!--idee.jpg"></div>-->
<!--    <div class="col s12 m4 l2"></div>-->
<!--    <div class="col s12 m4 l4"><p>-->
<!--        <h4 class="vie">La vie dans nos crèches</h4>-->
<!---->
<!--        MyCreche accompagne le développement psychomoteur, social et affectif des enfants dans un environnement sécurisé et pensé pour eux.-->
<!--        </p></div>-->
<!---->
<!--</div>-->



<?php
