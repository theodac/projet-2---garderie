
<?php
if (isset($_COOKIE['visite'])) {
    setCookie('visite', $_COOKIE['visite'] + 1 ,time() + 365*24*3600);
} else {
    setCookie('visite', 1 ,time() + 365*24*3600);
}
session_start();
?>
<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <link type="text/css" href="<?= PUB_PATH?>/public/css<?= DIRSEP;?>style.css"  rel="stylesheet" media="screen,projection"/>

    <!-- Compiled and minified CSS -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>


    <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>-->
    <!--<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script>-->

</head>
<body>


<!-- Navigation -->


<!-- Navigation -->

<nav class="navigation" role="navigation">


     <div class="nav-wrapper container"><a id="logo-container" href="<?= PUB_PATH;?>" class="brand-logo"><img src="<?= PUB_PATH;?>/public/img/logo.png" alt=""></a>

        <ul class="right hide-on-med-and-down">

            <?php
            if (isset($_SESSION['role']) && $_SESSION['role'] == 1){
                ?>
                <li><a href="<?= PUB_PATH;?>/administration/user">Administration</a></li>
                <?php
            }
            ?>
            <?php
            if (!isset($_SESSION['user'])) {
                ?>
                <li><a href="<?= PUB_PATH; ?>/inscription/index">Inscription</a></li>
                <li><a href="<?= PUB_PATH;?>/user/login">Connexion</a></li>

                <?php
            }else{

                if (isset($_SESSION['ROLE']) && $_SESSION['ROLE'] == 2  ){
                    ?>
                    <li><a href="<?= PUB_PATH;?>/profil/index">Profil</a></li>
                    <li><a href="<?= PUB_PATH;?>/profil/reservation">Mes Reservations</a></li>
                    <?php
                }
                ?>
                <?php
                if (isset($_SESSION['ROLE']) && $_SESSION['ROLE'] == 1 ){
                    ?>
                    <li><a href="<?= PUB_PATH;?>/profilPro/index">Profil</a></li>
                    <?php
                }
                if (isset($_SESSION['ROLE']) && $_SESSION['ROLE'] == 2){
                ?>

                <li><a href="<?= PUB_PATH;?>/recherche/index/1">Garderie</a></li>
                <?php
                }elseif (isset($_SESSION['ROLE']) && $_SESSION['ROLE'] == 1 ){
                    ?>
                    <li><a href="<?= PUB_PATH;?>/facture/index">Mes Factures</a></li>
                    <li><a href="<?= PUB_PATH;?>/chat/index">Messagerie</a></li>
                    <li><a href="<?= PUB_PATH;?>/detail/index/<?= $_SESSION['Name']?>_<?= $_SESSION['IDPRO']?>">Garderie</a></li>
                    <li><a href="<?= PUB_PATH;?>/planning/detail/<?= $_SESSION['Name']?>_<?= $_SESSION['IDPRO']?>">Planning</a></li>

                    <?php

                }
            if (isset($_SESSION['user'])) {
                ?>
                <li><a href="<?= PUB_PATH;?>/user/logout">Déconnexion</a></li>
                <?php
            }

            }
            ?>
        </ul>

         <ul class="sidenav" id="slide-out">
             <?php
             if (isset($_SESSION['role']) && $_SESSION['role'] == 1){
                 ?>
                 <li><a href="<?= PUB_PATH;?>/administration/user">Administration</a></li>
                 <?php
             }
             ?>
             <?php
             if (!isset($_SESSION['user'])) {
                 ?>
                 <li><a href="<?= PUB_PATH; ?>/inscription/index">Inscription</a></li>
                 <li><a href="<?= PUB_PATH;?>/user/login">Connexion</a></li>

                 <?php
             }else{

                 if (isset($_SESSION['ROLE']) && $_SESSION['ROLE'] == 2  ){
                     ?>
                     <li><a href="<?= PUB_PATH;?>/profil/index">Profil</a></li>

                     <?php
                 }
                 ?>
                 <?php
                 if (isset($_SESSION['ROLE']) && $_SESSION['ROLE'] == 1 ){
                     ?>
                     <li><a href="<?= PUB_PATH;?>/profilPro/index">Profil</a></li>

                     <?php
                 }
                 if (isset($_SESSION['ROLE']) && $_SESSION['ROLE'] == 2){
                     ?>

                     <li><a href="<?= PUB_PATH;?>/recherche/index/1">Garderie</a></li>
                     <?php
                 }elseif (isset($_SESSION['ROLE']) && $_SESSION['ROLE'] == 1 ){
                     ?>

                     <li><a href="<?= PUB_PATH;?>/profilPro/index">Profil</a></li>
                     <li><a href="<?= PUB_PATH;?>/facture/index">Mes Factures</a></li>
                     <li><a href="<?= PUB_PATH;?>/chat/index">Messagerie</a></li>
                     <li><a href="<?= PUB_PATH;?>/detail/index/<?= $_SESSION['Name']?>_<?= $_SESSION['IDPRO']?>">Garderie</a></li>

                     <?php

                 }
                 if (isset($_SESSION['user'])) {
                     ?>
                     <li><a href="<?= PUB_PATH;?>/user/logout">Déconnexion</a></li>
                     <?php
                 }

             }
             ?>
         </ul>
        <!--<ul id="nav-mobile" class="sidenav">
            <li><a href="#">Navbar Link</a></li>
            <li><a href="#">Navbar Link</a></li>
            <li><a href="#">Navbar Link</a></li>
            <li><a href="#">Navbar Link</a></li>
        </ul>-->
    </div>
    <div id="burgerCtn" data-target="slide-out">
        <span class="navigation__icon">&nbsp;</span>
    </div>
</nav>

