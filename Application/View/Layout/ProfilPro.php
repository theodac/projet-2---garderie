<h4> Profil du l'Utilisateur Professionel</h4>

<div class="container">
    <div class="section">


        <div class="row">
            <div class="col s12 m4">
                <div class="card">
                    <div class="card-image">
                        <img src="<?= PUB_PATH?>/public/img/<?= $pro['Picture']?>">
                    </div>
                </div>
            </div>

            <div class="col s12 m4">
                <div class="card">

                    <div class="card-content">
                        <ul>
                            <li> Nom de l'entreprise : <?= $pro['Name']?></li>
                            <li>Telephone : <?= $pro['Phone']?></li>
                            <li>Adresse : <?= $pro['Address']?></li>
                            <li>Code Postal : <?= $pro['ZIP']?></li>
                        </ul>
                    </div>


                </div>
                <a id="addInfo" class="waves-effect waves-light btn">Modifier</a>
            </div>

            <div class="col s12 m4">
                <div class="card">

                    <div class="card-content">
                        <h5>Informations diverses</h5>
                        <ul>
                            <li> Capacité d'accueil : <?= $pro['Capacity']?></li>
                            <li>Age minimum : <?= $pro['MinAge']?></li>
                            <li>Age maximum : <?= $pro['MaxAge']?></li>
                            <li>Prix : <?= $pro['Price']?></li>
                        </ul>
                    </div>


                </div>
            </div>
            <?php
            if ($_SESSION['user']['Secret'] == null) {
                ?>
                <a class="waves-effect waves-light btn modal-trigger" href="#modal1">Activer la double authentification</a>
                <?php
            }
            ?>
            <?php
            if ($_SESSION['user']['Secret'] !== null) {
                ?>
                <form method="post" action="">
                    <button type="submit" name="disableTfaPro" class="waves-effect waves-light btn">Désactiver la double authentification</button>
                </form>
                <?php
            }
            ?>

        </div>

    </div>





    <div id="modal1" class="modal">
        <form method="post" action="">
            <div class="modal-content" style="width: 24%; margin: 0 auto">
                <img src="<?= $qrCode ?>">

                <input type="text" name="verif"/>
                <input type="hidden" name="secretUser" value="<?= $secret ?>">
            </div>
            <div class="modal-footer">
                <button type="submit" name="submitTfa" class="waves-effect waves-light btn">Valider</button>
                <!--<a href="#" class="modal-close waves-effect waves-green btn-flat">Agree</a>-->
            </div>
        </form>
    </div>

    <script>
        $(document).ready(function(){
            $('.modal').modal();
        });
    </script>


    <div class="row" id="infoForm" style="display:none">
        <form class="col s12" method="post" action="#" enctype="multipart/form-data">
            <div class="row">

                <div class="input-field col s6">
                    <input id="Name" type="text" class="validate" name="name">
                    <label for="Name">Nom de l'entreprise  </label>
                </div>
                <div class="input-field col s6">
                    <input id="Phone" type="text" class="validate" name="phone">
                    <label for="Phone">Telephone</label>
                </div>
                <div class="input-field col s6">
                    <input id="email" type="text" class="validate" name="email">
                    <label for="email">Email  </label>
                </div>
                <div class="input-field col s6">
                    <input id="Address" type="text" class="validate" name="address">
                    <label for="Address">Adresse</label>
                </div>
                <div class="input-field col s6">
                    <input id="ZIP" type="text" class="validate" name="zip">
                    <label for="ZIP">Code Postal</label>
                </div>
                <div class="input-field col s6">
                    <input id="Capacity" type="text" class="validate" name="capacity">
                    <label for="Capacity">Capacité </label>
                </div>
                <div class="input-field col s6">
                    <input id="schedule" type="text" class="validate" name="schedule">
                    <label for="schedule">Schedule </label>
                </div>
                <div class="input-field col s6">
                    <input id="Price" type="text" class="validate" name="price">
                    <label for="Price">Prix</label>
                </div>
                <div class="input-field col s6">
                    <input id="MinAge" type="text" class="validate" name="minAge">
                    <label for="MinAge">Age Minimum</label>
                </div>
                <div class="input-field col s6">
                    <input id="MaxAge" type="text" class="validate" name="maxAge">
                    <label for="MaxAge">Age Maximum</label>
                </div>
                <div class="input-field col s6">
                    <input id="siret" type="text" class="validate" name="siret">
                    <label for="siret">Siret</label>
                </div>
                <div class="input-field col s6">
                    <textarea id="description"  class="validate materialize-textarea" name="description"></textarea>
                    <label for="description">Description</label>
                </div>
                <div class="file-field input-field col s6">
                    <div class="btn">
                        <span>Photo</span>
                        <input type="file" name="picture" id="picture">
                    </div>
                    <div class="file-path-wrapper">
                        <input class="file-path validate" type="text">
                    </div>
                </div>

                <button type="submit"  name="submit" class="waves-effect waves-light btn col s6">Valider</button>
                <button name="delete" id="delete" type="submit">Supprimer</button>



            </div>
        </form>



        <?php
        if(isset($_POST['delete'])){

            $test = $db->delete('professional ','WHERE ID='.$pro['ID']);
            session_destroy();

            ?>
            <script>
                document.location.href="http://localhost/projet-2-garderie/";
            </script>
        <?php
        }
        ?>


        <script>
            $('#addInfo').on('click', () => {
                if ($('#infoForm').attr("style")) {
                    $('#infoForm').fadeIn('slow', () => {
                        $('#infoForm').removeAttr("style")
                    })
                } else {
                    $('#infoForm').fadeOut('slow', () => {
                        $('#infoForm').attr("style", "display: none")
                    })
                }
            })
        </script>
    </div>


    <div class="row">
        <h4>Ajoutez vos plages de disponibilitées</h4>
        <form class="col s12" method="post" action="#" enctype="multipart/form-data">
            <div class="row">

                <div class="input-field col s6">
                    <input id="TimeBeg" type="datetime-local" class="validate" name="TimeBeg">
                </div>
                <div class="input-field col s6">
                    <input id="TimeEnd" type="datetime-local" class="validate" name="TimeEnd">
                </div>
                <div class="input-field col s6">
                    <input id="AvailableCap" type="number" class="validate" name="AvailableCap">
                </div>

                <button type="submit" name="submitDispo" class="waves-effect waves-light btn col s6">Valider</button>
            </div>
        </form>
</div>
</div>
