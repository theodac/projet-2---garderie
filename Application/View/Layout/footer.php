<footer class="page-footer footer">
    <div class="container">
        <div class="row">
            <div class="col l6 s12">
                <h5 class="white-text">My Creche</h5>
                <p class="grey-text text-lighten-4">
                    My Crèche vous accompagne dans le choix d'un mode de garde en France. Nos solutions d'accueil vous permettent de concilier harmonieusement votre vie professionnelle et familiale.

                    Notre réseau se compose de crèches du groupe Les Jeunes Pousses et de structures d'accueil de jeunes enfants partenaires.
                </p>
                <p>Ce site internet est réaliser dans le cadre d'un exercice </p>


            </div>
            <div class="col l3 s12">

            </div>

        </div>
    </div>
    <div class="footer-copyright">

    </div>
</footer>

<!-- Compiled and minified JavaScript -->
<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
<script src="<?= PUB_PATH?>/public/js<?= DIRSEP;?>script.js"></script>
<script src="<?= PUB_PATH?>/public/js<?= DIRSEP;?>admin.js"></script>
<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/pie.js"></script>
<script src="https://www.amcharts.com/lib/3/gauge.js"></script>
<script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
<link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
<script src="https://www.amcharts.com/lib/3/themes/light.js"></script>
<script>
    $('#chatBtn').on('click', () => {
        if ($('#reserve').attr("style")) {
        $('#reserve').fadeIn('slow', () => {
            $('#reserve').removeAttr("style")
    })
    } else {
        $('#reserve').fadeOut('slow', () => {
            $('#reserve').attr("style", "display: none")
    })
    }
    })
</script>

<script>
    $("#burgerCtn").on('click', () => {
        $('.sidenav').sidenav('open');
    })
</script>
<?php
include_once MDL_PATH.'Footer/Footer.php';
Footer::graphique();
?>
</body>
</html>