
<div class="main-content">
    <div class="formulaire">
        <div class="row">
            <div class="col-3"></div>
            <div class="col-6">
                <form class="register" action="" method="post">
                    <h3 class="my_title center-align">Inscription</h3>
                    <div class="center-align">
                            <button class="btn waves-effect waves-light  center-align" type="submit" name="action" id="partbtn" onclick="aff_part()">Particulier</button>
                            <button class="btn waves-effect waves-light  center-align" type="submit" name="action" id="probtn" onclick="aff_pro()">Professionnel</button>
                    </div>
                    <div class="part" id="part">
                        <?php require_once FORM_PATH."particulierForm.php"; ?>
                    </div>
                    <div class="pro" id="pro">
                        <?php require_once FORM_PATH."professionnalForm.php";?>
                    </div>
                </form>
            </div>
            <div class="col-3"></div>
        </div>
    </div>


</div>
<script type="text/javascript">



var divpro = document.getElementById('probtn')
divpro.addEventListener('click', (e) => {
    e.preventDefault();
    document.getElementById('pro').style.display = 'block';
    document.getElementById('part').style.display = 'none';

})

var divpro = document.getElementById('partbtn')
divpro.addEventListener('click', (e) => {
    e.preventDefault();
    document.getElementById('pro').style.display = 'none';
    document.getElementById('part').style.display = 'block';

})
document.getElementById('pro').style.display = 'none';
</script>
