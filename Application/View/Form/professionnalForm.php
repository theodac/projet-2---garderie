<form name="formPro" method="post" enctype="multipart/form-data">
<div class="input-field icon_prefix">
    <input id="name" type="text" class="validate" name="data[name]" value="" placeholder="Nom">
    <label for="name">Nom</label>
</div>
<div class="input-field icon_prefix">
    <input id="email" type="email" class="validate" name="data[email]" value="" placeholder="Email">
    <label for="name">Email</label>
    <span class="helper-text" data-error="Adresse email invalide" data-success="Adresse email valide"></span>
</div>
<div class="input-field icon_prefix">
    <input id="password" type="password" class="validate" name="data[password]" value="" placeholder="Mot de passe">
    <label for="password">Mot de passe</label>
</div>
<div class="input-field icon_prefix">
    <input id="confirmPass" type="password" class="validate" name="passwordConfirm" value="" placeholder="Confirmation du mot de passe">
    <label for="confirmPass">Confirmation du mot de passe</label>
</div>
<div class="input-field icon_prefix">
    <input id="phone" type="tel" class="validate" name="data[phone]" value="" placeholder="Numéro de téléphone">
    <label for="phone">Numéro de téléphone</label>
</div>
<div class="input-field icon_prefix">
    <input id="address" type="text" class="validate" name="data[address]" value="" placeholder="Adresse">
    <label for="address">Adresse</label>
</div>
<div class="input-field icon_prefix">
    <input id="zipcode" type="text" class="validate" name="data[zip]" value="" placeholder="ZIP">
    <label for="zipcode">Code postal</label>
</div>
<div class="input-field icon_prefix">
    <input id="schedule" type="text" class="validate" name="data[schedule]" value="" placeholder="Disponibilite">
    <label for="schedule">Disponibilité </label>
</div>
<div class="input-field icon_prefix">
    <input id="price" type="number" class="validate" name="data[price]" value="" placeholder="prix">
    <label for="price">Prix par heure</label>
</div>
<div class="input-field icon_prefix">
    <input id="capacity" type="text" class="validate" name="data[capacity]" value="" placeholder="capacité">
    <label for="capacity">Capacité maximale</label>
</div>
<div class="input-field icon_prefix">
    <input id="minAge" type="number" class="validate" name="data[minage]" value="" placeholder="Age minimum">
    <label for="minAge">Age minimum </label>
</div>
<div class="input-field icon_prefix">
    <input id="maxAge" type="number" class="validate" name="data[maxage]" value="" placeholder="Age maximum">
    <label for="maxAge">Age maximale</label>
</div>
<div class="input-field icon_prefix">
    <input id="siret" type="text" class="validate" name="data[siret]" value="" placeholder="SIRET">
    <label for="siret">Numéro de SIRET</label>
</div>
<div class="file-field input-field">
      <div class="btn">
        <span>Photo</span>
        <input type="file" name="picture" id="picture">
      </div>
      <div class="file-path-wrapper">
        <input class="file-path validate" type="text">
      </div>
    </div>
<div class="center-align">
    <button class="btn waves-effect waves-light" type="submit" name="actionPro">Validez
        <i class="material-icons right">send</i>
    </button>
</div>
</form>
