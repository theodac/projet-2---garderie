<header>

    <nav>
        <div class="nav-wrapper">
                    <ul id="nav-mobile" class="left hide-on-med-and-down">
                        <li><a href="<?= PUB_PATH;?>/administration/user">User</a></li>
                        <li><a href="<?= PUB_PATH;?>/administration/manage">Administration</a></li>
                    </ul>
        </div>
    </nav>
</header>

<main>

    <div class="row">
        <div class="col s6">
            <div style="padding: 35px;" align="center" class="card">
                <div class="row">
                    <div class="left card-title">
                        <b>Dernier Utilisateur inscrit</b>
                    </div>
                </div>

                <div class="row">
                    <table>
                        <tr>
                            <th>Nom</th>
                            <th>Telephone</th>
                            <th>Email</th>
                            <th>Addresse</th>
                            <th>Code Postal</th>
                        </tr>
                    <?php
                    while ($d = $user->fetch()){

                        ?>

                         <tr>

                     <td><?= $d['Name'] ." ". $d['Firstname'] ?></td>
                    <td><?= $d['Phone'] ?></td>
                    <td><?= $d['Email'] ?></td>
                    <td><?= $d['Address'] ?></td>
                    <td><?= $d['ZIP'] ?></td>
                         </tr>

                    <?php
                    }
                    ?>


                    </table>

                </div>
            </div>
        </div>

        <div class="col s6">
            <div style="padding: 35px;" align="center" class="card">
                <div class="row">
                    <div class="left card-title">
                        <b>Dernière Creche inscrite</b>
                    </div>
                </div>
                <div class="row">
                    <table>
                        <tr>
                            <th>Nom</th>
                            <th>Telephone</th>
                            <th>Place</th>
                            <th>Addresse</th>
                            <th>Code Postal</th>
                        </tr>
                        <?php
                        while ($d = $pro->fetch()){
                            ?>

                            <tr>

                                <td><?= $d['Name']  ?></td>
                                <td><?= $d['Phone'] ?></td>
                                <td><?= $d['Capacity'] ?></td>
                                <td><?= $d['Address'] ?></td>
                                <td><?= $d['ZIP'] ?></td>
                            </tr>

                            <?php
                        }
                        ?>


                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col s6">
            <div style="padding: 35px;" align="center" class="card">
                <div class="row">
                    <div class="left card-title">
                        <b>Graphique</b>
                    </div>
                </div>

                <div class="row">
                    <div id="chartdiv"></div>
                </div>
            </div>
        </div>

        <div class="col s6">
            <div style="padding: 35px;" align="center" class="card">
                <div class="row">
                    <div class="left card-title">
                        <b>Traffic de votre site</b>
                    </div>
                </div>
                <div class="row">
                    <div id="chartCookie"></div>
                </div>
            </div>
        </div>
    </div>


</main>
