<div class="content">
<div class="row">
    <div class="col s1"></div>
    <div class="col s5 left-align">
        <?=
            $sql["pro"]["Name"]."</br>".
            $sql["pro"]["Address"]."</br>".
            $sql["pro"]["Phone"]."</br>"
         ?>
    </div>
    <div class="col s5 right-align">
        <?=
            $sql["user"]["Firstname"]." ".
            $sql["user"]["Name"]."</br>".
            $sql["user"]["Address"]."</br>"
         ?>
    </div>
    <div class="col s1"></div>
</div>
<div class="row">
    <h3 class="center-align">FACTURE DU MOIS DE <?= $month["month"] ?></h3>
    <div class="col s1"></div>
    <div class="col s4">
        Designation <hr></br>
        <?php
        $cpt = 0;
        foreach ($sql["child"] as $value)
        {
            echo "Garde de ".$value["Firstname"]."</br>";
            $cpt++;
        }
        ?>
    </div>
    <div class="col s2">
        Prix <hr></br>
        <?php
            for ($i=0; $i < $cpt ; $i++)
            {
                echo $sql["fact"]["Price"]."€</br>";
            }
        ?>
    </div>
    <div class="col s2">
        Nombre d'heures <hr></br>
        <?php
        foreach ($child as $value)
        {
            echo $value["hours"].'</br>';
        }
        ?>
    </div>
    <div class="col s2">
        Total <hr></br>
        <?php
            foreach ($total as $value)
            {
                echo $value."€</br>";
            }
        ?>
    </div>
    <div class="col s1"></div>
</div>
<div class="row">
    <div class="col s1"></div>
    <div class="col s6">
        Facture n°: <?= $sql["fact"]["Libelle"] ?></br>
        A régler avant le <?= date('d/m/Y', strtotime($month["paidDate"])); ?>
    </div>
    <div class="col s2">
        Heure total : <?= $total_hours.' h'; ?></br>
    </div>
    <div class="col s2">
        Total : <?= $total_price.'€'; ?></br>
    </div>
    <div class="col s1"></div>

<form class="" action="" method="post">
    <button type="submit" formtarget="_blank" name="submit">
        Download
    </button>
</form>
</div>
</div>