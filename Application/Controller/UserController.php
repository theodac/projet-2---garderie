<?php
/**
 * Created by PhpStorm.
 * User: WEBENOO
 * Date: 05/11/2018
 * Time: 14:24
 */

class UserController extends Framework
{
    public function login()
    {
        include './Application/View/connexion.php';
        include './Application/Model/User/User.php';

        if(isset($_POST['action'])) {
            $data = $_POST['data'];
            foreach ($data as $key => $value) {
                if(empty($value)) {
                    ?><script>alert('Veuillez bien remplir le formulaire')</script><?php
                    exit;
                }
            }
        User::login($data);
        }

    }

    public function verify()
    {
        include './Application/View/verifyAccount.php';
        include './Application/Model/User/User.php';

        if(isset($_POST['action'])) {
            $data = $_POST['data'];
            foreach ($data as $key => $value) {
                if(empty($value)) {
                    ?><script>alert('Veuillez bien remplir le formulaire')</script><?php
                    exit;
                }
            }
            User::verify($data);
        }
    }

    public function passwordForgot()
    {
        include './Application/View/passwordForgot.php';
        include MDL_PATH .'/User/User.php';

        if(isset($_POST['action'])) {
            User::passwordForgot($_POST['data']);
        }
    }

    public function logout()
    {
        include MDL_PATH .'/User/User.php';
        User::logout();
    }

    public function tfacheck() {
        include MDL_PATH .'/User/User.php';
        $this->render('doubleAuthCheck');
        User::tfacheck();
    }

}