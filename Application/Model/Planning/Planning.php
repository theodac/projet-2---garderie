<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 27/11/2018
 * Time: 13:58
 */

class Planning extends Framework
{
public function __construct()
{
}
public static function planning(){
    $getParamUrl= $_SERVER['REQUEST_URI'];
    $getParamUrlArray = explode('_',$getParamUrl);
    $id = $getParamUrlArray[1];
    $model = new Model();
    $reservation = $model->select('reservation','WHERE CHILD_has_PROFESSIONAL_PROFESSIONAL_ID='.$id);
    $reservation = $reservation->fetchAll();
    $cap = new Model();
    $cap = $cap->select('availability', 'WHERE PROFESSIONAL_ID = '.$id);
    $cap = $cap->fetch();

    $data = [
        'reservation'=>$reservation,
        'cap'=>$cap
    ];
    return $data;
}


}