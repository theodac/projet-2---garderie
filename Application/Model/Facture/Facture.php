<?php
/**
 * Created by PhpStorm.
 * User: WEBENOO
 * Date: 25/11/2018
 * Time: 21:59
 */

class Facture
{
    public static function index(){
        $sql = new Model();
        $sql = $sql->select('invoice', 'WHERE PROFESSIONAL_ID ='.$_SESSION['IDPRO']);
        $sql = $sql->fetchAll();
        return $sql;
    }
    public static function detail($id){

        $fact = new Model();
        $fact = $fact->select('invoice', 'WHERE ID ='.$id);
        $fact = $fact->fetch();

        $pro = new Model();
        $pro = $pro->select('professional', 'WHERE ID ='.$fact["PROFESSIONAL_ID"]);
        $pro = $pro->fetch();

        $user = new Model();
        $user = $user->select('user', 'WHERE ID ='.$fact["USER_ID"]);
        $user = $user->fetch();

        $child = new Model();
        // $child = $child->requete("SELECT * FROM child WHERE USER_ID =".$fact["USER_ID"]);
        $child = $child->select('child',"WHERE USER_ID =".$fact["USER_ID"]);
        $child = $child->fetchAll();

        $date = $fact["Date"];
        $month = date("m",strtotime($date));
        var_dump($month);
        $month--;
        $planning = array();

        $id = 0;
        foreach ($child as $value)
        {
            $plan = new Model();
            $plan = $plan->select('reservation',
                "WHERE CHILD_has_PROFESSIONAL_CHILD_ID =".$value['ID'].
                " AND CHILD_has_PROFESSIONAL_PROFESSIONAL_ID = ".$fact['PROFESSIONAL_ID'].
                " AND MONTH(Start_event) = ".$month);
            $plan = $plan->fetchAll();
            $planning[$id] =$plan;
            $id++;
        }
        // var_dump($planning);

        $facture = array(
            'fact' => $fact,
            'pro' => $pro,
            'user' => $user,
            'child' => $child,
            'plan' => $planning
        );
        return $facture;
    }

    public static function Get_Month($facture)
    {
        $month = array(
           'month' => '',
           'paidDate' => ''
        );
        $date = explode('-', $facture["fact"]["Date"]);
        switch ($date[1]) {
            case '01':

                $month["month"] = "JANVIER";
                if(date("L", strtotime($date)) == TRUE)
                {
                    $month["paidDate"] = implode("-", $date);
                }
                else
                {
                    $month["paidDate"] = implode("-", $date);
                }
                break;
            case '02':
                $month["month"] = "FEVRIER";
                $month["paidDate"] = implode("-", $date);
                break;
            case '03':
                $month["month"] = "MARS";
                $month["paidDate"] = implode("-", $date);
                break;
            case '04':
                $month["month"] = "AVRIL";
                $month["paidDate"] = implode("-", $date);
                break;
            case '05':
                $month["month"] = "MAI";
                $month["paidDate"] = implode("-", $date);
                break;
            case '06':
                $month["month"] = "JUIN";
                $month["paidDate"] = implode("-", $date);
                break;
            case '07':
                $month["month"] = "JUILLET";
                $date[1] = "31";
                $month["paidDate"] = implode("-", $date);
                break;
            case '08':
                $month["month"] = "AOUT";
                $month["paidDate"] = implode("-", $date);
                break;
            case '09':
                $month["month"] = "SEPTEMBRE";
                $month["paidDate"] = implode("-", $date);
                break;
            case '10':
                $month["month"] = "OCTOBRE";

                $month["paidDate"] = implode("-", $date);
                break;
            case '11':
                $month["month"] = "NOVEMBRE";
                $date[1] = 11;


                $month["paidDate"] = implode("-", $date);
                break;
            case '12':
                $month["month"] = "DECEMBRE";
                $month["paidDate"] = implode("-", $date);
                break;
        }
        return $month;
    }

    public static function Get_Current_Hours($child)
    {
       $hour = 0;
       foreach ($child as $value)
       {
           $date_debut = date("Y/m/d H:i:s",strtotime(str_replace('-','/', $value["Start_event"])));
           $date_fin = date("Y/m/d H:i:s",strtotime(str_replace('-','/', $value["End_event"])));
           $date_result = date_diff( new DateTime($date_debut),new DateTime($date_fin));
           $time = $date_result->format("%h");
           $hour += $time;
       }
       return $hour;
    }

    public static function Get_Current_Childs_Hours($sql)
    {
        $child = array();
        $hours = array(
            'hours' => ''
        );
        $cpt = 0;
        foreach ($sql["plan"] as $value)
        {
            $hours["hours"] = Facture::Get_Current_Hours($value);
            $child[$cpt] = $hours;
            $cpt++;
        }
        return($child);
    }

    public static function Get_Total($sql)
    {
        $total = array();
        $tmp = array(
            'total' => ''
        );
        $cpt = 0;

        foreach ($sql["plan"] as $child)
        {
            $nb_hours = Facture::Get_Current_Hours($child);
            $price = $sql["fact"]["Price"];

            $tmp = $price*$nb_hours;
            $total[$cpt] = $tmp;
            $cpt++;
        }
        return $total;
    }

    public static function Get_Hours_Total($total)
    {
        $total_hours = 0;
        foreach($total as $hour)
        {
            $total_hours += intval($hour["hours"]);
        }
        $final = strval($total_hours);
        return $final;
    }

    public static function Get_Total_Final($hour, $facture)
    {
        $total = 0;
        foreach ($hour as $value)
        {
            $total += $value["hours"] * $facture["fact"]["Price"];
        }
        $final = strval($total);
        return $final;
    }
}
