<?php
/**
 * Created by PhpStorm.
 * User: WEBENOO
 * Date: 05/11/2018
 * Time: 14:25
 */

class User extends Model
{


    public function __construct()
    {

    }


    public static function register($arr)
    {
        $db = new Model();
        $email = $arr['email'];
        $emailExistUser = $db->requete("SELECT * FROM user WHERE Email = '$email'")->fetch();
        $emailExistPro = $db->requete("SELECT * FROM professional WHERE Email = '$email'")->fetch();

        if ($arr['password'] != $_POST['passwordConfirm']) {
            echo 'Les deux mots de passes ne correspondent pas';
        } else if (mb_strlen($arr['password']) < 7) {
            echo 'Votre mot de passe doit contenir au minimum 7 caractères';
        } else if ($emailExistUser || $emailExistPro ) {
            echo 'Adresse email déjà utilisée';
        } else {
            $welcome = 'bienvenue !' . "\r\n";
            $link = "\r\n" . 'http://localhost/projet-2-garderie/user/verify';
            $usrpswd = $arr['password'];
            //var_dump($usrpswd);
            $hashpswd = password_hash($usrpswd, PASSWORD_DEFAULT);
            $arr['password'] = $hashpswd;

            // Génération de la clé de vérification
            $random = random_bytes(8);
            $verifKey = bin2hex($random);
            $arr['verify'] = $verifKey;

            try {
                $usrprep = $db->preparation('INSERT INTO user (ID,Name, Firstname, Email, Phone, Address, ZIP, Status, Password, ROLE_ID, Others , Verify, Actif,Secret, Geolocalisation) VALUES (NULL ,:name, :firstname, :email, :phone, :address, :zip, :status, :password, 2, NULL ,:verify, 0, NULL , :geolocalisation )');
                foreach ($arr as $key => &$value) {
                    $usrprep->bindParam(":" . $key, $value);
                }
                $tmp = $arr['address'];
                $zip = $arr['zip'];
                $url = 'https://nominatim.openstreetmap.org/search/'.rawurlencode($tmp). '%20' .rawurlencode($zip).'?format=json&limit=1&email=axel.aubry69@gmail.com';
                $file = file_get_contents($url);
                $json = json_decode($file, true);
                $geoloc = $json[0]["lat"].",".$json[0]["lon"];
                $usrprep->bindParam(':geolocalisation', $geoloc);
                $usrprep->execute();

            } catch(PDOException $e) {
                echo $e->getMessage();
            }

            /*--------------ESSAI MAILCATCHER---------------*/
            //$welcome = 'Bienvenue, votre clé pour activer votre compte est : '
            $to      = $arr['email'];
            $subject = 'le sujet';
            $message = $welcome . $arr['verify'] . $link;
            $headers = 'From: webmaster@creche.com' . "\r\n" .
                'Reply-To: webmaster@example.com' . "\r\n" .
                'X-Mailer: PHP/' . phpversion();

            mail($to, $subject, $message, $headers);
            /*----------------------------------------------*/

            // Redirection vers la page d'activation

            ?>
            <script>
                document.location.href="http://localhost/projet-2-garderie/user/verify";
            </script>
            <?php


        }

    }

    public static function registerPro($arr)
    {
        $db = new Model();
        $email = $arr['email'];
        $emailExistPro = $db->requete("SELECT * FROM professional WHERE Email = '$email'")->fetch();
        $emailExistUser = $db->requete("SELECT * FROM user WHERE Email = '$email'")->fetch();

        if ($arr['password'] != $_POST['passwordConfirm']) {
            echo 'Les deux mots de passes ne correspondent pas';
        } else if (mb_strlen($arr['password']) < 7) {
            echo 'Votre mot de passe doit contenir au minimum 7 caractères';
        } else if ($emailExistUser || $emailExistPro) {
            echo 'Adresse email déjà utilisée';
        } else {
            $welcome = 'bienvenue !' . "\r\n";
            $link = "\r\n" . 'http://localhost/projet-2-garderie/user/verify';
            $propswd = $arr['password'];
            //var_dump($propswd);
            $hashpswd = password_hash($propswd, PASSWORD_DEFAULT);
            $arr['password'] = $hashpswd;

            $random = random_bytes(8);
            $verifKey = bin2hex($random);
            $arr['verify'] = $verifKey;

            try {
                $proprep = $db->preparation('INSERT INTO professional (Name, Phone, Email, Address, ZIP, Schedule, Capacity, MinAge, MaxAge, SIRET, Price, Picture, Geographical, Password, Verify, Actif) VALUES (:name, :phone, :email, :address, :zip, :schedule, :capacity, :minage, :maxage, :siret, :price, :picture, :geographical, :password, :verify, 0)');
                foreach ($arr as $key => &$value) {
                    $proprep->bindParam(":" . $key, $value);
                    /*
                    move_uploaded_file($_FILES['picture']['tmp_name'],$_FILES['picture']['name']);
                    fopen(PUB_PATH,'r');
                    rename(ROOT.$_FILES['picture']['name'],ROOT.'public/img/'.$_FILES['picture']['name']);*/
                }
                $tmp = $arr['address'];
                $zip = $arr['zip'];
                $url = 'https://nominatim.openstreetmap.org/search/'.rawurlencode($tmp). '%20' . rawurlencode($zip) .'?format=json&limit=1&email=axel.aubry69@gmail.com';
                $file = file_get_contents($url);
                $json = json_decode($file, true);
                $geoloc = $json[0]["lat"].",".$json[0]["lon"];
                $proprep->bindParam(':geographical', $geoloc);
                $proprep->bindParam(':picture',$_FILES['picture']['name']);
                move_uploaded_file($_FILES['picture']['tmp_name'],$_FILES['picture']['name']);
                fopen(PUB_PATH,'r');
                rename(ROOT.$_FILES['picture']['name'],ROOT.'public/img/'.$_FILES['picture']['name']);
                $proprep->execute();




            } catch(PDOException $e) {
                echo $e->getMessage();
            }

            /*--------------ESSAI MAILCATCHER---------------*/
            //$welcome = 'Bienvenue, votre clé pour activer votre compte est : '
            $to      = $arr['email'];
            $subject = 'le sujet';
            $message = $welcome . $arr['verify'] . $link;
            $headers = 'From: webmaster@creche.com' . "\r\n" .
                'Reply-To: webmaster@example.com' . "\r\n" .
                'X-Mailer: PHP/' . phpversion();

            mail($to, $subject, $message, $headers);
            /*----------------------------------------------*/
            ?>
            <script>
                document.location.href="http://localhost/projet-2-garderie/user/verify";
            </script>
            <?php

            ?>

            <?php
        }
    }

    public static function login($arr)
    {
        $usrpswd = $arr['password'];
        $usremail = $arr['email'];
        /*$hashpswd = password_hash($usrpswd, PASSWORD_DEFAULT);
        $arr['password'] = $hashpswd;*/

        //var_dump($usremail);

        try {
            $db = new Model();
            $isActifUser = $db->requete("SELECT Actif FROM user WHERE Email = '$usremail'")->fetch();
            $isActifPro = $db->requete("SELECT Actif FROM professional WHERE Email = '$usremail'")->fetch();
            $emailUserExists = $db->requete("SELECT * FROM user WHERE Email = '$usremail'")->fetch();
            $emailProExists = $db->requete("SELECT * FROM professional WHERE Email = '$usremail'")->fetch();
            //var_dump($isActifPro['Verify']);

            if ($emailUserExists == false && $emailProExists == false) {
                echo 'Adresse email inexistante dans note base de données';
            } else if ($emailUserExists != false) {
                if ($isActifUser['Actif'] == 0) {
                    echo "Votre compte est inactif, veuillez l'activer";
                } else {
                    $usrhash = $db->requete("SELECT Password FROM user WHERE Email = '$usremail'")->fetch();
                    if ($usrhash == false) {
                        echo "Adresse email invalide";
                    } else {
                        if (password_verify($usrpswd, $usrhash[0])) {
                            $usrlog = $db->requete("SELECT * FROM user WHERE Email = '$usremail' AND Password = '$usrhash[0]'")->fetchAll(PDO::FETCH_ASSOC);
                            if($usrlog[0]['Secret'] != null) {
                                $_SESSION['Secret'] = $usrlog[0]['Secret'];
                                ?>
                                <script>
                                    document.location.href="http://localhost/projet-2-garderie/user/tfacheck";
                                </script>
                                <?php
                            } else {
                                $_SESSION['user'] = $usrlog[0];
                                $_SESSION['role'] = $_SESSION['user']['ROLE_ID'];
                                $_SESSION['ID'] = $_SESSION['user']['ID'];
                                $_SESSION['ROLE'] = 2;
                                $_SESSION['Geographical'] = $_SESSION['user']['Geolocalisation'];
                                ?>
                                <script>
                                    document.location.href="http://localhost/projet-2-garderie/";
                                </script>
                                <?php
                            }
                        }  else {
                            echo 'Mot de passe invalide';
                        }
                    }
                }
            } else if ($emailProExists != false) {
                if ($isActifPro['Actif'] == 0) {
                    echo "Votre compte est inactif, veuillez l'activer";
                } else {
                    $usrhash = $db->requete("SELECT Password FROM professional WHERE Email = '$usremail'")->fetch();
                    if ($usrhash == false) {
                        echo "Adresse email invalide";
                    } else {
                        if (password_verify($usrpswd, $usrhash[0])) {
                            $usrlog = $db->requete("SELECT * FROM professional WHERE Email = '$usremail' AND Password = '$usrhash[0]'")->fetchAll(PDO::FETCH_ASSOC);
                            //var_dump($usrlog);
                            if($usrlog[0]['Secret'] != null) {
                                $_SESSION['Secret'] = $usrlog[0]['Secret'];
                                ?>
                                <script>
                                    document.location.href="http://localhost/projet-2-garderie/user/tfacheck";
                                </script>
                                <?php
                            } else {
                                $_SESSION['user'] = $usrlog[0];
                                $_SESSION['IDPRO'] = $_SESSION['user']['ID'];
                                $_SESSION['Name'] = $_SESSION['user']['Name'];
                                $_SESSION['ROLE'] = 1;


                                ?>
                                <script>
                                    document.location.href="http://localhost/projet-2-garderie/";
                                </script>
                                <?php
                            }
                        } else {
                            echo 'Mot de passe invalide';
                        }
                    }
                }
            } else {
                echo "Une erreur s'est produite, veuillez retenter";
            }
        } catch(PDOException $e) {
            echo $e->getMessage();
        }

    }

    public static function verify($arr)
    {
        $db = new Model();
        $usrEmail = $arr['email'];
        $usrKey = $arr['code'];
        $keyQueryUser = $db->requete("SELECT Verify FROM user WHERE Email = '$usrEmail'")->fetch();
        $keyQueryPro = $db->requete("SELECT Verify FROM professional WHERE Email = '$usrEmail'")->fetch();

        if ($usrKey == $keyQueryUser['Verify']) {
            $db->requete("UPDATE user SET Actif = 1 WHERE Verify = '$usrKey'");
            ?>
            <script>
                document.location.href="http://localhost/projet-2-garderie/user/login";
            </script>
            <?php
        } else if ($usrKey == $keyQueryPro['Verify']) {
            $db->requete("UPDATE professional SET Actif = 1 WHERE Verify = '$usrKey'");
            ?>
            <script>
                document.location.href="http://localhost/projet-2-garderie/user/login";
            </script>
            <?php
        } else {
            echo 'Mauvais email ou code';
        }
    }

    public static function passwordForgot($arr)
    {
        if (!empty($arr)) {
            $db = new Model();
            $emailVal = $arr['email'];
            $keyVal = $arr['key'];
            //$emailUserExists = $db->query("SELECT * FROM USER WHERE Email = '$emailVal'");
            $keyUserExists = $db->requete("SELECT Verify FROM user WHERE Email = '$emailVal'")->fetch();


            if($keyUserExists['Verify'] == false) {
                $keyProExists = $db->requete("SELECT Verify FROM professional WHERE Email = '$emailVal'")->fetch();

                if ($keyProExists['Verify'] != false && $keyVal != $keyProExists['Verify']) {
                    echo "La clé entrée n'est pas valide";
                } else if ($keyProExists['Verify'] == false) {
                    echo "Adresse mail ou clé invalide";
                } else {
                    $random = random_bytes(8);
                    $newPass = bin2hex($random);
                    $hashPass = password_hash($newPass, PASSWORD_DEFAULT);


                    $welcome = 'Bonjour, votre nouveau mdp est :' . "\r\n";
                    $pass = "\r\n" . $newPass;
                    $to      = $emailVal;
                    $subject = 'le sujet';
                    $message = $welcome . $pass;
                    $headers = 'From: webmaster@creche.com' . "\r\n" .
                        'Reply-To: webmaster@example.com' . "\r\n" .
                        'X-Mailer: PHP/' . phpversion();

                    mail($to, $subject, $message, $headers);

                    $db->requete("UPDATE professional SET Password = '$hashPass' WHERE Email = '$emailVal'");

                    echo 'réinitialisation de votre mdp réussi, regardez vos mails';
                }

            } elseif ($keyUserExists['Verify'] != false && $keyVal != $keyUserExists['Verify']) {
                echo "La clé entrée n'est pas valide";
            } else {

                $random = random_bytes(8);
                $newPass = bin2hex($random);
                $hashPass = password_hash($newPass, PASSWORD_DEFAULT);


                $welcome = 'Bonjour, votre nouveau mdp est :' . "\r\n";
                $pass = "\r\n" . $newPass;
                $to      = $emailVal;
                $subject = 'le sujet';
                $message = $welcome . $pass;
                $headers = 'From: webmaster@creche.com' . "\r\n" .
                    'Reply-To: webmaster@example.com' . "\r\n" .
                    'X-Mailer: PHP/' . phpversion();

                mail($to, $subject, $message, $headers);

                $db->requete("UPDATE user SET Password = '$hashPass' WHERE Email = '$emailVal'");

                echo 'réinitialisation de votre mdp réussi, regardez vos mails';
            }
        }
    }

    public static function logout()
    {
        session_destroy();
        $_SESSION['user'] = null;
        ?>
        <script>
            document.location.href="http://localhost/projet-2-garderie/";
        </script>
<?php
    }

    public static function tfacheck()
    {
        if(isset($_POST['action'])) {
            include 'vendor/autoload.php';
            $verify = new \RobThree\Auth\TwoFactorAuth();
            if($verify->verifyCode($_SESSION['Secret'], $_POST['secret'])) {
                $secret = $_SESSION['Secret'];
                $model = new Model();
                $usrlog = $model->select('user', " WHERE Secret = '$secret'")->fetchAll(PDO::FETCH_ASSOC);
                if ($usrlog) {
                    $_SESSION['user'] = $usrlog[0];
                    $_SESSION['role'] = $_SESSION['user']['ROLE_ID'];
                    $_SESSION['ID'] = $_SESSION['user']['ID'];
                    $_SESSION['ROLE'] = 2;
                    $_SESSION['Secret'] = null;
                } else {
                    $usrlog = $model->select('professional', " WHERE Secret = '$secret'")->fetchAll(PDO::FETCH_ASSOC);

                    if($usrlog) {
                        $_SESSION['user'] = $usrlog[0];
                        $_SESSION['IDPRO'] = $_SESSION['user']['ID'];
                        $_SESSION['Name'] = $_SESSION['user']['Name'];
                        $_SESSION['ROLE'] = 1;
                    } else {
                        echo 'Utilisateur non trouvé';
                    }
                }

                ?>
                <script>
                    document.location.href="http://localhost<?=PUB_PATH?>";
                </script>
                <?php
            } else {
                ?>
                <script>alert('Code erroné, veuillez réessayer')</script>
                <?php
            }
        }
    }


}
