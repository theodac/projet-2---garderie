<?php
/**
 * Created by PhpStorm.
 * User: WEBENOO
 * Date: 22/11/2018
 * Time: 14:15
 */

class ProfilPro
{
    public function __construct()
    {
    }
    public static function profilPro(){
        include 'vendor/autoload.php';
        $model = new Model();
        $pro= $model->select('professional','WHERE ID='.$_SESSION['IDPRO']);
        $pro = $pro->fetch();
        $db = $model;
        $tfa = new \RobThree\Auth\TwoFactorAuth('MyCreche');
        $secret = $tfa->createSecret('160');
        $qrCode = $tfa->getQRCodeImageAsDataUri($_SESSION['user']['Email'], $secret);
        if (isset($_POST['submit'])){
            if(isset($_POST['name']) && isset($_POST['phone']) && isset($_POST['email']) && isset($_POST['address']) && isset($_POST['zip']) && isset($_POST['schedule']) && isset($_POST['capacity']) && isset($_POST['minAge'])&& isset($_POST['maxAge'])&& isset($_POST['siret'])&& isset($_POST['price'])&& isset($_FILES['picture'])){

                $model->update('professional','Name=\''.$_POST['name'].'\', Phone=\''.$_POST['phone'].'\', Email=\''.$_POST['email'].'\', Address=\''.$_POST['address'].'\', ZIP=\''.$_POST['zip'].'\', Schedule=\''.$_POST['schedule'].'\', Capacity='.$_POST['capacity'].', MinAge='.$_POST['minAge'].', MaxAge='.$_POST['maxAge'].', SIRET='.$_POST['siret'].', Price='.$_POST['price'].', Picture=\''.$_FILES['picture']['name'].'\',Description=\''.$_POST['description'].'\'',' WHERE ID='.$_SESSION['IDPRO']);
                move_uploaded_file($_FILES['picture']['tmp_name'],$_FILES['picture']['name']);
                fopen(PUB_PATH,'r');
                rename(ROOT.$_FILES['picture']['name'],ROOT.'public/img/'.$_FILES['picture']['name']);
                ?>
                <script>
                    document.location.href="http://localhost/projet-2-garderie/profilPro/index";
                </script>

                <?php

            }else{
                echo '<span>Veuillez remplir tous les champs </span>';
            }


        }
        if (isset($_POST['submitDispo'])){
            $model->insert('availability', ' TimeBeg, TimeEnd, AvailableCap, PROFESSIONAL_ID' ,
                '\''.$_POST['TimeBeg'].'\',\''.$_POST['TimeEnd'].'\',\''.$_POST['AvailableCap'].'\', '.$_SESSION['IDPRO']);
            ?>
            <script>
                document.location.href="http://localhost/projet-2-garderie/profilPro/index";
            </script>

            <?php
        };

        if(isset($_POST['submitTfa'])) {
            $verif = $_POST['verif'];
            $secretUser = $_POST['secretUser'];
            $userId = $_SESSION['user']['ID'];

            if($tfa->verifyCode($secretUser, $verif)) {
                $model->update('professional', 'Secret=\''. $secretUser . '\'', ' WHERE ID ='.$userId);
                $_SESSION['user']['Secret'] = $secretUser;
                ?>
                <script>alert('La double authentification a été activée avec succès')</script>
                <?php
            } else {
                ?>
                <script>alert('Une erreur s\'est produite, veuillez réessayer')</script>
                <?php

            }
        }

        if(isset($_POST['disableTfaPro'])) {
            $userId = $_SESSION['user']['ID'];
            $_SESSION['user']['Secret'] = null;
            $model = new Model();
            $model->update('professional', 'Secret= NULL', ' WHERE ID ='.$userId);
        }
        $data = [
            'pro'=>$pro,
            'db' => $db,
            'secret' => $secret,
            'qrCode' => $qrCode
        ];
        return $data;


    }

}