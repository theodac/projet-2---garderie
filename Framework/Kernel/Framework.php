<?php
/**
 * Created by PhpStorm.
 * User: WEBENOO
 * Date: 07/11/2018
 * Time: 15:41
 */

require_once './Application/Controller/UserController.php';
require_once './Application/Controller/ProfilController.php';
require_once './Application/Controller/ProfilProController.php';
require_once './Application/Controller/AdministrationController.php';
require_once './Application/Controller/InscriptionController.php';
require_once './Application/Controller/DetailController.php';
require_once './Application/Controller/RechercheController.php';
require_once './Application/Controller/PDFController.php';
require_once './Application/Controller/ChatController.php';
require_once './Application/Controller/FactureController.php';
require_once './Application/Controller/PlanningController.php';

class Framework
{
    private $_viewparams;

    public static function run()
    {
        self::initialize();
        self::autoloader();
        self::header();
        self::switcher();
        self::footer();
        self::bot();

    }

    private static function initialize()
    {
        $getParamUrl= $_SERVER['REQUEST_URI'];
        $getParamUrlArray = explode("/",$getParamUrl);


        define('DIRSEP',DIRECTORY_SEPARATOR);
        define('ROOT', getcwd().DIRSEP);
        define('APPPATH',ROOT.'Application'.DIRSEP);
        define('FRAMEWORK_PATH',ROOT.'Framework'.DIRSEP);
        define('PUB_PATH',dirname($_SERVER['SCRIPT_NAME']));
        define('CTRL_PATH', APPPATH. 'Controller'.DIRSEP);
        define('MDL_PATH', APPPATH. 'Model'.DIRSEP);
        define('VIEW_PATH', APPPATH. 'View'.DIRSEP);
        define('FORM_PATH', VIEW_PATH . 'Form'.DIRSEP);
        define('LAYOUT_PATH', VIEW_PATH . 'Layout'.DIRSEP);
        define('UNKNOWN_PAGE', LAYOUT_PATH . '404.php');
        define('KERNEL_PATH',FRAMEWORK_PATH. 'Kernel'.DIRSEP);
        define('HEADER_PATH', LAYOUT_PATH.'header.php');
        define('FOOTER_PATH', LAYOUT_PATH.'footer.php');
        define('BOT_PATH', LAYOUT_PATH.'bot.php');
        if (count($getParamUrlArray) == 5 || count($getParamUrlArray) == 4 ) {
            if ($getParamUrlArray[2] != "" && $getParamUrlArray[3] != "") {
                define('CONTROLLER', $getParamUrlArray[2]);
                define('ACTION', $getParamUrlArray[3]);
                if (isset($getParamUrlArray[4])&&$getParamUrlArray[4] != "") {
                    define('SLUG', $getParamUrlArray[4]);
                }
                else{
                    define('SLUG', '');
                }
            }
        }

    }
    private static function autoloader()
    {
        spl_autoload_register(array(__CLASS__,'loading'));
    }
    private static function loading($class)
    {
        if (substr($class,-10) == "Controller"){
            require_once "Framework.php";
        }
        elseif (substr($class,-5) == "Model"){
            require_once "Framework.php";
        }
    }

    private static function switcher()
    {
        $getParamUrl = $_SERVER['REQUEST_URI'];
        $getParamUrlArray = explode("/", $getParamUrl);

        if (count($getParamUrlArray) == 3 && $getParamUrlArray[2] == "") {
            include LAYOUT_PATH . "accueil.php";
        }else{
            if (isset($getParamUrlArray[3])) {
                if ($getParamUrlArray[2] != "" && $getParamUrlArray[3] != "") {
                    if (file_exists(CTRL_PATH . CONTROLLER . "Controller.php")) {
                        $controllerName = CONTROLLER . "Controller";
                        $actionName = ACTION;

                        $controller = new $controllerName;
                        if (method_exists($controller, ACTION)) {
                            $controller->$actionName();
                        } else {
                            include UNKNOWN_PAGE;
                        }
                    } else {
                        include UNKNOWN_PAGE;
                    }
                } else {
                    include UNKNOWN_PAGE;

                }
            } else {
                include UNKNOWN_PAGE;

            }
        }
    }
    /**
     * Permet de générer l'affichage
     * de la vue passé en paramètre.
     * @param $view Vue à afficher.
     * @param array $viewparam Données à passer à la vue.
     */
    protected function render(string $view, Array $viewparams = []) {
        # Récupération et Affectation des Paramètres de la Vue
        $this->_viewparams = $viewparams;
        # Permet d'accéder au tableau directement dans des variables
        extract($this->_viewparams);
        # Chargement de la Vue
        $view = VIEW_PATH . '/' . $view . '.php';
        if( file_exists($view) ) :
            # Chargement de la Vue
            include_once $view;
        else :
            $this->render('Layout/404', [
                'message' => 'Aucune vue correspondante'
            ]);
        endif;
    }
    private static function footer(){
        include_once FOOTER_PATH;
    }
    private static function bot(){
        include_once BOT_PATH;
    }
    private static function header(){
    include_once HEADER_PATH;
    }

}
