// HEADER
(function($){
    $(function(){

        $('.sidenav').sidenav();

    }); // end of document ready
})(jQuery); // end of jQuery name space



// CAROUSEL
var instance = M.Carousel.init({
    fullWidth: true,
    indicators: true
});

// Or with jQuery

$('.carousel.carousel-slider').carousel({
    fullWidth: true,
    indicators: true
});
autoplay()
function autoplay() {
    $('.carousel').carousel('next');
    setTimeout(autoplay, 4500);
};

